#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from read_data import non_empty_lines, login_pass



def freq1(path):
    passwords = { } # or like this dict()
    seq = non_empty_lines(path)
    seq = login_pass(seq) # move seq from first to second 

    for _, pwd in seq:  # login make like this _,  
        if pwd in passwords:
            passwords[pwd] += 1
        else: 
            passwords[pwd] = 1
    
    
    passwords = list( passwords.items() )
    passwords.sort(key=lambda x: x[1], reverse = True)
    if len(passwords) < 10: 
        return passwords
    return passwords[:10]
