#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from urllib.request import urlopen

class DownLoader(object):
    
    def __init__(self, url):
        self.__url = url
    
    def __enter__(self):
        print('Enter into context')
        result = urlopen(self.__url).read()
        return result
        
    def __exit__(self, exctype, excvalue, traceback):
        if exctype is not None:
            result = issubclass(exctype, IndexError)
        else:
            return None
        print('Exit from context')
        return result 
    
    
b = DownLoader('http://www.yandex.ru')
with b as page:
    print(len(page))
    
with b as page:
    print('\n Second example')
    raise IndexError()
    print('Continue of second example')

with b as page:
    print('\n Third exapmle')
    raise ZeroDivisionError()
    print('Continue of third example')
    
