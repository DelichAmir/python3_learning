#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from urllib.request import urlopen
from datetime import date

def data_lines(url, encoding='utf-8'):
    with urlopen(url) as extsrc:
        data = b''
        while True:
            chunk = extsrc.read(1024)
            if not chunk: break
            data += chunk 
            while b'\n' in data:
                n = data.index(b'\n')
                yield data[:n].decode(encoding)
                data = data[n+1:]
    if data:
        yield data.decode(encoding)


DATE = re.compile(r'''
                  <td>
                  (\d\d)        # day
                  \.
                  (\d\d)        # mounth
                  \.
                  (\d\d\d\d)    #year
                  </td>''', re.X)
        
SCALE = re.compile(r'<td>(\d+)</td>')

COURSE = re.compile(r'''
                    (\d+)
                    [,\.]
                    (\d\d\d\d)
                    </td>''', re.X)


def data_from(seq):
    adate = ascale = acourse = None
    
    for line in seq:
        M = DATE.search(line)  # if not fine return None(it is False)
        if M:
            # Make analise of find 
            day, mounth, year = map(int, M.groups())
            adate = date(year,mounth, day)
            continue
        M = SCALE.search(line)
        if M:
            # Make analise of find 
            scale, = M.groups()
            ascale = int(scale) 
            continue
        M = COURSE.search(line)
        if M:
            # Make analise of find 
            a, b = M.groups()
            acourse = float(f'{a}.{b}')
            yield (adate, ascale, acourse)
            adate = ascale = acourse =  None
            continue
        
        pass # Maybe we will write somthing else 
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
