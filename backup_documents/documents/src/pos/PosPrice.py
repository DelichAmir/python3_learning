#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .Position import Position


class PosPrice(Position):
    
    def __init__(self, title, quantity, unit=None, price=None):
        
        #super(PosPrice, self).__init__(title, quantity, unit)
        super().__init__(title, quantity, unit)
        self.__price = price
    
    @property
    def price(self):
        return self.__price
    
    @property
    def summa(self):
        return self.quantity * self.price
    
    
    @summa.setter
    def summa(self, new_summa):
        q = new_summa / self.price
        self._set_quantity(q)


class PosPriceInt(PosPrice):
    
    @property
    def summa(self):
        return super().summa
    
    @summa.setter
    def summa(self, new_summa):
        q = new_summa // self.price
        self._set_quantity( int(q) )
