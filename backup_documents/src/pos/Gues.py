#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from .Position import Position
from .PosPrice import PosPrice
from .PosSumma import PosSumma

def guess(**kwargs):
    if 'price' in kwargs:
        return PosPrice
    if 'summa' in kwargs:
        return PosSumma
    return Position

