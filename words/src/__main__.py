#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from pathlib import Path as p
from all_words import all_words

filepath = (p.cwd() / 'data' / 'vim1.txt').absolute()

DIGITS_ONLY = re.compile(r'^\d+$')


words = dict()
seq = all_words(filepath, encoding= 'windows-1251')
seq = ( x for x in seq if x )   # if x - it is False cause epmty string is False 
seq = ( x for x in seq if DIGITS_ONLY.search(x) is None )

for w in seq:
    if w in words:
        words[w] += 1 
    else: 
        words[w] = 1
    
for w,n in words.items():
    print(w, n)
