#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os.path  # old way with route to file 
from pathlib import Path  # New way to find way to file 

filename = 'data.txt'

filepath = Path.cwd() / filename  # this is route to file
filepath = filepath.absolute()
print(filepath)


with filepath.open('rt',encoding='utf-8') as src:
    # Read all data from file 
    '''
    for x in src:
        # x = x.rstrip()
        x = str.rstrip(x)
        print(x)
    '''
    
    for x in map(str.rstrip, src):
        print(x)
    
print('---')
