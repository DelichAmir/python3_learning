#!/usr/bin/env python3
# -*- coding: utf-8 -*-


filename = 'data.txt'

f = open(filename, 'wt', encoding='utf-8')    # w: write, r: read, b: byte, t: text 

try:
    while True:
        text = input(':')
        if text.strip() == '':   # strip() -delete all spacess  from start and end of line
            break       # break entering text
        if text.strip()[0] == '#':
            continue    # continue from while 
        print(text, file=f)

finally:
    f.close()
