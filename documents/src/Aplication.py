#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Nakladnaya import Nakladnaya
import shelve
from contextlib import contextmanager
from network import Server
from threading import Thread




class Application(object):
    
    def __init__(self, filepath, port=8081):
        
        self.__filepath = filepath
        self.__current = None
        self.__server = None
        self.__port = port
        self.__dispath = {
            'c': self.create_new,
            'p': self.print_current,
            'a': self.add_position,
            'l': self.list_all,
            's': self.select
        }
        self.start_numbering()
    
    
    @property
    def repository(self):
        return shelve.open(self.__filepath)
    
    
    def save(self):
        with self.repository as repo:
            repo[str(self.__current.number)] = self.__current
    
    
    def print_current(self):
        if self.__current is None:
            print('\nThere is no data')
        self.__current.out()
        
    
    def add_position(self):
        if self.__current is None:
            print('\nThere is no data')
        
        else:
            params = {}
            
            title = input("Please input the 'title' of position: ").strip()
            if title == '':
                return
            params['title'] = title
            
            quantity = input("Please input the 'quantity' of unit: ").strip()
            if quantity == '':
                return
            params['quantity'] = float(quantity)
            
            unit = input("Please input the 'unit' of item: ").strip()
            if unit == '':
                return
            params['unit'] = unit
            
            price = input("Please input the 'price' of item: ").strip()
            if price != '':
                params['price'] = float(price)
            
            summa = input("Please input the 'summa' of items: ").strip()
            if summa != '':
                params['summa'] = float(summa)
                
            self.__current.add_pos(**params)
            self.save()
                
    
    def create_new(self):
        self.__current = x = Nakladnaya()
        self.save()
        print(x)
    
    def list_all(self):
        with self.repository as repo:
            for n in repo.values():
                print(n)
                
    def select(self):
        num = input("Enter number of 'Nakladnaya': ").strip()
        with self.repository as repo:
            try:
                self.__current = repo[num]
            except KeyError:
                print(f'There is no current number [ {num} ] ')
                
    def start_numbering(self):
        with self.repository as repo:
            mn = max(map(int, repo.keys()))
            Nakladnaya.set_first_free_number(mn+1)
    
    @contextmanager
    def runserver(self):
        self.__server = Server(application = self, port= self.__port)
        thread = Thread(None, self.__server.serve_forever)
        thread.start()
        try:
            yield self.__server
            
        finally:
            self.__server.shutdown()
            thread.join()
            self.__server = None 
        
        
    def execute(self):
        with self.runserver():
            
            green = lambda text: '\033[0;32m' + text + '\033[0m'
            red = lambda text: '\033[0;31m' + text + '\033[0m'
        
            while True:
                print('\n')
                print('###########################################################################')
                print('#  Welcome to CLI of program: Work                         # version 1.0  #')
                print('###########################################################################')
                print('# \t\t\t{}                                     #'.format( green('Instruction: ') ) )
                print("# enter  ' {} ' to create a new 'Nakladnaya'                               #".format( green('c') ) )
                print("# enter  ' {} ' to add position to 'Nakladnaya'                            #".format( green('a') ) )
                print("# enter  ' {} ' to send 'Nakladnaya to server                              #".format( green('r') ) )
                print("# enter  ' {} ' to print  'Nakladnaya                                      #".format( green('p') ) )
                print("# enter  ' {} ' to print list of all 'Nakladnaya'                          #".format( green('l') ) )
                print("# enter  ' {} ' to select 'Nakladnaya                                      #".format( green('s') ) )
                print("# enter  ' {} ' to exit the programm                                       #".format( green('q') ) )
                print('###########################################################################')
                choice = input('Your choice: ').lower() 
            
            
                if choice == 'q':
                    print('{}'.format(red('\nGoodbye ...') ) )
                    break
            
                try:
                    method = self.__dispath[choice]
                    method()
                    
                except KeyError:
                    print('{}'.format(red ('\nIncorrect choice [ {} ]'.format(choice)) ) )
                

    
            
