#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from http.server import HTTPServer, BaseHTTPRequestHandler


class Server(HTTPServer):
    
    def __init__(self, application, port):
        
        super().__init__(('',port), Handler)
        self.__aplication = application  # Error find
        
        
    @property
    def app(self):
        return self.__aplication
    
class Handler(BaseHTTPRequestHandler):
    
    def do_POST(self):
        
        print('Nakladnaya recived ...')
        self.send_response(200, 'OK')
        self.end_headers()
        self.wfile.write('OK'.encode('utf-8'))
        
        
    # For test
    do_GET = do_POST
    
