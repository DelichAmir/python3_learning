#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod, abstractproperty 

# ABCMeta отслеживание абстрактности  класса и всех дочерных классов

class Position(object, metaclass=ABCMeta):
    
    # TODO добавить свойство is_good =
    #  True  - если правильная 
    #  False - если некорректная 
    
    def __init__(self, title, quantity=1, unit=None):
        self.__title = title
        if unit is not None:
            self.__quantity = quantity
            self.__unit = unit
        else:
            self.__quantity = None 
            self.__unit = None 
    
    @abstractproperty
    def price(self):
        return None
    
    @abstractproperty
    def summa(self):
        return None 
    
    
    @property
    def title(self):
        return self.__title
    
    @property
    def quantity(self):
        return self.__quantity
    
    # protected method
    def _set_quantity(self, new_quantity):
        self.__quantity = new_quantity
    
    @property
    def unit(self):
        return self.__unit
    
    def out(self):
        print(f'{self.title} {self.quantity} {self.price} {self.summa}' )
        
    
