#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from .read import input_one
from .write import output_all
